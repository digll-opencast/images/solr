## Docker Solr ##

This repository contains build instructions for a debian-based image running an Apache Solr instance
in version **1.4.1** using its embedded Jetty Servlet-Engine.

Note that this software is quite outdated! (*2010-06-24*)

### Building the image ###

Building the image can be achieved with the collowing commands:
```bash
git clone https://gitlab.digll-hessen.de/OC-Cluster/images/solr.git
cd solr

# Using docker daemin
sudo docker build --tag solr:1.4.1 -f Dockerfile .
```

(*Replace `buildah build-using-dockerfile` with `docker build` when using the docker deamon*)

### Publishing the image ###

The image should not be uploaded to the official Docker hub due to its outdated software and low reusability.
The image should however to pushed to your local (*gitlab*) image registry for use in your container infrastructure.

Manually pushing image using *buildah* tool (after having been build):
```bash
sudo buildah push solr:master docker://registry.gitlab.com/digll-opencast/images/solr:master
```
(*Replace `buildah` with `docker` when using the docker deamon*)

This repository contains Gitlab CI-Runner job configurations for building and pushing build-artifacts
to your local container registry, assuming it has support for this. Images will be tagged according to branch-name.

### Using the image ###

The image can be pulled and started with the following commands:
```bash
sudo podman pull registry.gitlab.com/digll-opencast/images/solr:master
sudo podman run -p 8983:8983 solr:master
```
(*Replace `buildah` with `docker` when using the docker deamon*)

### Configuration ###

This image does currently not offer much in the way of configuration,
since the default should be enough for most use-cases.
The Apache Solr application can be found under `/opt/solr`, the Jetty-configuration
is stored in `/opt/solr/etc`. (*See assets for defaults.*)

Note that this image by default does not come with any pre-configured cores.
Cores can be added by mounting them into the `/opt/solr/solr` directory, eg.
```--volume=/path/to/your/core/:/opt/solr/solr```
where `/path/to/your/core/` should look something like this:

```
/path/to/your/core/
├── bin
├── conf
│   ├── protwords.txt
│   ├── schema.xml
│   ├── solrconfig.xml
│   ├── stopwords.txt
│   └── synonyms.txt
└── lib
    └── opencast-solr-8.6.jar
```

This version of Apache Solr also supports multiple cores. See the `example/multicore` directory
in the Apache Solr release tarbal for more information.

The image exposes a Jetty-Server running Apache-Solr as a servlet on (inside) port 8983.
When the port if exposed to the host an admin-interface can be reached under <http://localhost:8983/solr/>

### Persistence ###

The data storage location is controlled by the `<dataDir>...</dataDir>` directive in your cores
`/opt/solr/solr/[core-name/]conf/solrconfig.xml`.
To have the data persisted you should attach a volume to the directory mentioned inside this directive.

### Updating assets ###

The assets where extracted from the Apache Solr version **1.4.1** release tarbal hosted
on the Apache Archive mirror under <https://archive.apache.org/dist/lucene/solr/>, specifically
[this](https://archive.apache.org/dist/lucene/solr/1.4.1/apache-solr-1.4.1.tgz) file.

Most of the asset files come from the Jetty-Example project located inside the `example` directory,
but where stripped from non essential files!
