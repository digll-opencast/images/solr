# Images requires Java runtime
# Note: Later version do start, but fail to compile cores!
FROM openjdk:8-jre

# Copy apache solr version 1.4.1 into container
# Note: This version is implemented as jetty-application
COPY ./assets/ "/opt/solr"

# Ensure log-directory exists
RUN mkdir -p "/opt/solr/logs"

# Expose default jetty port used by apache solr
EXPOSE 8983

# Configure application entry-point and startup arguments
WORKDIR /opt/solr/
ENTRYPOINT [ "/usr/bin/env" ]
CMD [ "java", "-jar", "/opt/solr/start.jar" ]
